FROM golang:1.21 as builder
WORKDIR go
COPY catgpt . 
RUN go mod download && CGO_ENABLED=0 go build -o "/catgpt"
FROM gcr.io/distroless/static-debian12:latest-amd64
COPY --from=builder catgpt /
EXPOSE 8080 9090
CMD ["/catgpt"]